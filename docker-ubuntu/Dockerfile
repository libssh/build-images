# using latest is intentional for CI
# hadolint ignore=DL3007
FROM ubuntu:latest

LABEL maintainer="Jakub Jelen <jjelen@redhat.com>"

# This is required because Ubuntu installation is interactive
# even if the -y is provided (sic)
ENV DEBIAN_FRONTEND noninteractive

# using latest versions is intentional for CI
# hadolint ignore=DL3008
RUN apt-get update \
 && apt-get install -y \
        build-essential \
        cmake \
        clang \
        clang-tools \
        doxygen \
        dropbear \
        gcc \
        git \
        lcov \
        libgcrypt20-dev \
        llvm \
        make \
        libasan5 \
        libcmocka-dev \
        libfido2-dev \
        libmbedtls-dev \
        libnss-wrapper \
        libpam-wrapper \
        libp11-dev \
        libresolv-wrapper \
        libsocket-wrapper \
        libssl-dev \
        libubsan1 \
        libuid-wrapper \
        libz-dev \
        ncat \
        netcat-traditional \
        openssh-client \
        openssh-server \
        softhsm2 \
        valgrind \
        krb5-kdc \
        krb5-admin-server \
        krb5-user \
        libkrb5-dev \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
# This directory is needed for openssh server to start. It is probably
# supposed to be created by tmpfiles, but they will not work in containers
 && mkdir /run/sshd
